package com.example.demo;


import org.springframework.http.HttpStatus;
        import org.springframework.http.ResponseEntity;
        import org.springframework.web.bind.annotation.*;

        import java.util.HashMap;

@RestController
public class WidgetController {

    private HashMap<Long,Widget> repository  = new HashMap<Long,Widget>();
    private long localId = 0l;

    @GetMapping
    public ResponseEntity index(){
        return new ResponseEntity(repository.values(), HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity get(@PathVariable Long id){
        return new ResponseEntity(repository.get(id),HttpStatus.OK);
    }

    @PostMapping
    public String create(@RequestBody Widget widget){
        widget.setId(++localId);
        repository.put(widget.getId(),widget);
        return "Widget "+widget+" Created";
    }

    @PutMapping("{id}")
    public ResponseEntity update(@RequestBody Widget widget, @PathVariable Long id){
        if(repository.containsKey(id)){
            widget.setId(id);
            repository.replace(id,widget);
        }

        return new ResponseEntity(widget,HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public String remove(@PathVariable Long id){
        repository.remove(id);
        return "Widget having ID = "+id+ " is removed";
    }
}